import { inject } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivateFn, Router, RouterStateSnapshot } from '@angular/router';

export const authGuard: CanActivateFn = (
      next: ActivatedRouteSnapshot,
      state: RouterStateSnapshot
    ) => {
      const router = inject(Router)
      const token = localStorage.getItem('access_token')
      if (token) {
          return true
      } else {
          return router.parseUrl('/login')
    }

}
