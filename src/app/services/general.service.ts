import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class GeneralService {

  constructor(
    private httpClient: HttpClient
  ) { }

  registrar(data: any) {
    return this.httpClient.post(`http://localhost:3030/users`, data)
  }

}
