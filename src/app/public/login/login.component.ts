import { Component } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent {

  loginForm = new FormGroup({
    email: new FormControl('', Validators.required),
    senha: new FormControl('', Validators.required) 
  })

  constructor(
      private authService: AuthService,
      private router: Router
    ) {

  }

  async logar() {
    this.authService.authLogin(this.loginForm.value).subscribe(
      (response: any) => {
        console.log(response)
        if (response && response.access_token) {
          localStorage.setItem('access_token', response.access_token)
          this.router.navigate(['/dashboard'])
        } else {
          alert('erro ao logar')
        }
    },
      (error) => {
        console.error(error)
        alert('error')
      }
    )
  }

}
