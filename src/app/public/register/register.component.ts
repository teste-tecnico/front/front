import { Component } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { GeneralService } from 'src/app/services/general.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent {

  registerForm = new FormGroup({
    nome: new FormControl('', Validators.required),
    email: new FormControl('', Validators.required),
    telefone: new FormControl('', Validators.required),
    senha: new FormControl('', Validators.required),
  })

  constructor(
    private generalService: GeneralService,
    private router: Router
  ) { }

  registrar() {
    this.generalService.registrar(this.registerForm.value).subscribe(
      (response: any) => {
        console.log(response)
        if (response) {
          this.router.navigate(['/login'])
          setTimeout(() => {
            alert('user criado')
          }, 1000)
        } else {
          alert('erro ao registrar')
        }
      },
      (error) => {
        console.error(error)
        alert('error')
      }
    ) 
  }

}
